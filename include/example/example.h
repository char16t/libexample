// Copyright 2015 Valeriy Manenkov <v.manenkov@gmail.com>

#ifndef INCLUDE_LIBEXAMPLE_LIBEXAMPLE_H_
#define INCLUDE_LIBEXAMPLE_LIBEXAMPLE_H_

class example_t {
 public:
    example_t();
    int flag;
};

#endif  // INCLUDE_LIBEXAMPLE_LIBEXAMPLE_H_