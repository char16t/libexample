#include <gtest/gtest.h>
#include "example/example.h"

TEST(main_test, first_test)
{
   example_t a;
   ASSERT_EQ(a.flag, 1);

   a.flag = 2;
   ASSERT_EQ(a.flag, 2);
}